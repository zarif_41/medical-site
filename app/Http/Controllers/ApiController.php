<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Adress;
use App\Comment;
use App\Info;
use App\FAQ;
use App\News;
use App\Pattents;
use App\UPhoto;
use App\Uslug;
use App\Spec;
use Mail;

class ApiController extends Controller
{
    public function Index(Request $req)
    {
        $method = $req->method;
        return $this->$method($req);
    }

    private function addresses()
    {
        return Adress::get();
    }

    private function comments()
    {
        return Comment::get();
    }

    private function info()
    {
        return Info::first();
    }

    private function faq()
    {
        return FAQ::get();
    }

    private function news($req)
    {
        return News::get()->forPage($req->page, $req->count);
    }

    private function searchNews($req)
    {
        return News::where('name', 'like', '%' . $req->search . '%')
                    ->orWhere('article', 'like', '%' . $req->search . '%')
                    ->get();
    }

    private function pattents()
    {
        return Pattents::get();
    }

    private function specs()
    {
        return Spec::get();
    }

    private function spec($r)
    {
        return Spec::where('id', $r->id)->first();
    }

    private function photos($r)
    {
        return UPhoto::where('spec', $r->spec)->get();
    }

    private function uslug($r)
    {
        return Uslug::where('spec', $r->spec)->get();
    }

    private function sendMail($r)
    {
        $mail = $r->mail['name'] . ' оставил заявку на сайте. Номер телефона: ' . $r->mail['number'];

        Mail::raw($mail, function($message)
        {
            $message->to(env('MY_MAIL'));
        });
    }

}
