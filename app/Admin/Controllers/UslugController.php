<?php

namespace App\Admin\Controllers;

use App\Uslug;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Spec;


class UslugController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Услуги';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Uslug);

        $grid->column('id', __('№'));
        $grid->column('spec', __('Специализация'));
        $grid->column('name', __('Имя'));
        $grid->column('price', __('Цена'));
        $grid->column('created_at', __('Создано'));
        $grid->column('updated_at', __('Изменено'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Uslug::findOrFail($id));

        $show->field('id', __('№'));
        $show->field('spec', __('Специализация'));
        $show->field('name', __('Имя'));
        $show->field('price', __('Цена'));
        $show->field('created_at', __('Создано'));
        $show->field('updated_at', __('Изменено'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Uslug);

        $form->select('spec', __('Специализация'))->options($this->getSpecs());
        $form->text('name', __('Имя'));
        $form->number('price', __('Цена'));

        return $form;
    }

    private function getSpecs() 
    {
        $specs = Spec::get();
        $response = [];
        foreach($specs as $row) {
            $response[$row->id] = $row->name;
        }
        return $response;
    }
}
