<?php

namespace App\Admin\Controllers;

use App\Pattents;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PattentsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Патенты и награды';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Pattents);

        $grid->column('id', __('№'));
        $grid->column('category', __('Категория'));
        $grid->column('name', __('Имя'));
        $grid->column('photo',__('Фото'));
        $grid->column('created_at', __('Создано'));
        $grid->column('updated_at', __('Изменено'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Pattents::findOrFail($id));

        $show->field('id', __('№'));
        $show->field('category', __('Категория'));
        $show->field('name', __('Имя'));
        $show->field('photo',__('Фото'));
        $show->field('created_at', __('Создано'));
        $show->field('updated_at', __('Изменено'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Pattents);

        $form->select('category', __('Категория'))->options(['Награды', 'Патенты']);
        $form->text('name', __('Имя'));
        $form->image('photo',__('Фото'));

        return $form;
    }
}
