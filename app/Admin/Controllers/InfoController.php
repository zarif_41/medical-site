<?php

namespace App\Admin\Controllers;

use App\Info;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class InfoController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Общая инфа';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Info);

        $grid->column('id', __('№'));
        $grid->column('phone', __('Телефон'));
        $grid->column('mail', __('Mail'));
        $grid->column('vk', __('Vk'));
        $grid->column('insta', __('Insta'));
        $grid->column('fb', __('Fb'));
        $grid->column('created_at', __('Создано'));
        $grid->column('updated_at', __('Изменено'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Info::findOrFail($id));

        $show->field('id', __('№'));
        $show->field('phone', __('Телефон'));
        $show->field('mail', __('Mail'));
        $show->field('vk', __('Vk'));
        $show->field('insta', __('Insta'));
        $show->field('fb', __('Fb'));
        $show->field('created_at', __('Создано'));
        $show->field('updated_at', __('Изменено'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Info);

        $form->text('phone', __('Телефон'));
        $form->email('mail', __('Mail'));
        $form->text('vk', __('Vk'));
        $form->text('insta', __('Insta'));
        $form->text('fb', __('Fb'));

        return $form;
    }
}
