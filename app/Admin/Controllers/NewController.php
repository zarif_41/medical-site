<?php

namespace App\Admin\Controllers;

use App\News;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class NewController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\News';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new News);

        $grid->column('id', __('№'));
        $grid->column('name', __('Имя'));
        $grid->column('photo',__('Фото'));
        $grid->column('category', __('Категория'));
        $grid->column('article',__('Статья'));
        $grid->column('created_at', __('Создано'));
        $grid->column('updated_at', __('Изменено'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(News::findOrFail($id));

        $show->field('id', __('№'));
        $show->field('name', __('Имя'));
        $show->field('photo',__('Фото'));
        $show->field('category', __('Категория'));
        $show->field('article',__('Статья'));
        $show->field('created_at', __('Создано'));
        $show->field('updated_at', __('Изменено'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new News);

        $form->text('name', __('Имя'));
        $form->image('photo',__('Фото'));
        $form->select('category', __('Категория'))->options(['Акции' ,'Хирургам']);
        $form->textarea('article',__('Статья'));

        return $form;
    }
}
