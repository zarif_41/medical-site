<?php

namespace App\Admin\Controllers;

use App\Adress;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class AdressController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Адреса';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Adress);

        $grid->column('id', __('№'));
        $grid->column('address', __('Адрес'));
        $grid->column('phone', __('Телефон'));
        $grid->column('workhourse', __('Раб.часы'));
        $grid->column('category', __('Категория'));
        $grid->column('title', __('Титул'));
        $grid->column('created_at', __('Создано'));
        $grid->column('updated_at', __('Изменено'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Adress::findOrFail($id));

        $show->field('id', __('№'));
        $show->field('address', __('Адрес'));
        $show->field('phone', __('Телефон'));
        $show->field('workhourse', __('Раб.часы'));
        $show->field('category', __('Категория'));
        $show->field('title', __('Титул'));
        $show->field('created_at', __('Создано'));
        $show->field('updated_at', __('Изменено'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Adress);

        $form->text('address', __('Адрес'));
        $form->text('phone', __('Телефон'));
        $form->text('workhourse', __('Раб.часы'));
        $form->select('category', __('Категория'))->options(['Прием', 'Операции']);
        $form->text('title', __('Титул'));

        return $form;
    }
}
