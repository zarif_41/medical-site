<?php

namespace App\Admin\Controllers;

use App\Spec;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SpecController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Специализация';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Spec);

        $grid->column('id', __('№'));
        $grid->column('name', __('Имя'));
        $grid->column('desc', __('Описание'));
        $grid->column('created_at', __('Создано'));
        $grid->column('updated_at', __('Изменено'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Spec::findOrFail($id));

        $show->field('id', __('№'));
        $show->field('name', __('Имя'));
        $show->field('desc', __('Описание'));
        $show->field('created_at', __('Создано'));
        $show->field('updated_at', __('Изменено'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Spec);

        $form->text('name', __('Имя'));
        $form->textarea('desc', __('Описание'));

        return $form;
    }
}
