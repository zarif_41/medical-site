<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');
    $router->resource('adresses', AdressController::class);
    $router->resource('comments', CommentController::class);
    $router->resource('f-a-qs', FAQController::class);
    $router->resource('infos', InfoController::class);
    $router->resource('news', NewController::class);
    $router->resource('pattents', PattentsController::class);
    $router->resource('specs', SpecController::class);
    $router->resource('u-photos', UPhotoController::class);
    $router->resource('uslugs', UslugController::class);

});
