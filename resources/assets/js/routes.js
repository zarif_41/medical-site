import Home from './views/Home'
import Contacts from './views/Contacts'
import News from './views/News'
import About from './views/About'
import Specs from './views/Specs'
import Uslug from './views/Uslug'
import Info from './views/Info'
import Faq from './views/Faq'
import Search from './views/Search'

const routes = [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/contacts',
            name: 'contacts',
            component: Contacts,
            meta: {
                title: 'Контакты',
            }
        },
        {
            path: '/news',
            name: 'news',
            component: News,
            meta: {
                title: 'Новости',
            }
        },
        {
            path: '/about',
            name: 'about',
            component: About,
            meta: {
                title: 'О хирурге',
            }
        },
        {
            path: '/info',
            name: 'info',
            component: Info,
            meta: {
                title: 'Полезная информация для пациента',
            }
        },
        {
            path: '/faq',
            name: 'faq',
            component: Faq,
            meta: {
                title: 'Часто задаваемые вопросы',
            }
        },
        {
            path: '/search',
            name: 'search',
            component: Search,
            meta: {
                title: 'Поиск',
            }
        },
        {
            path: '/specs',
            name: 'specs',
            component: Specs,
            meta: {
                title: 'Услуги и цены',
            }
        },
        {
            path: '/specs/:id',
            name: 'uslug',
            component: Uslug,
            meta: {
                title: 'Услуги и цены',
                parent: '/specs',
            }
        },
];

export default routes
