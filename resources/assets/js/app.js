import "./bootstrap";
import Vue from "vue";
import VueRouter from "vue-router";
import YmapPlugin from "vue-yandex-maps";
import routes from "./routes";
import App from "./App.vue";
import Element from 'element-ui';
import '../sass/theme/index.css';
import moment from 'moment';

moment.locale('ru');
Vue.use(Element);
Vue.use(VueRouter);
Vue.use(YmapPlugin);

const router = new VueRouter({
    mode: "history",
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    },
    routes: routes
});

const app = new Vue({
    el: "#app",
    router,
    render: h => h(App)
});
